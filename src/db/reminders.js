const mongoose = require('mongoose');
const {Schema}=mongoose;


const RemindersSchema=new Schema({
    user:{type: String, required: true},
    phone:{type: Number, required: true},
    time:{type: Number, required: true},
    description:{type: String, required: true},
})


module.exports=mongoose.model('Reminders', RemindersSchema)