const express = require("express");
const router = express.Router();
const auth=require("./auth")
const dbRedis=require("../db/redis")


const wa = require('@open-wa/wa-automate');
let clientWA 
let timeOutIDs=[]
init()


//db
const Task=require('../db/reminders');
const Users=require('../db/users');


router.get("/",secure, getTasks);
router.get("/:id",secure, getTasks);
router.post('/',secure,insertTask)
router.put('/:id',secure, updateTask)
router.delete('/:id',secure,deleteTask)
router.post('/login/',login)
router.post('/register/',register)
router.post('/validatelogin/',secure,(req,res,next)=>{
    res.status(200).json({username:req.body.userjwt})
})


async function getTasks(req, res){
    let task
    if(req.params.id){
        task= await Task.find({_id:req.params.id,user:req.body.userjwt})
    }else{
        task= await Task.find({user:req.body.userjwt})
    }
    res.json(task)
}
async function insertTask(req, res){
    let {phone,time,description,userjwt}=req.body;    
    
    if(!isNaN(phone)&&!isNaN(time)&&description.length>0){
        const task = new Task({phone,time,description,user:userjwt})
        await task.save();     

        createCron(time,phone,description)
        res.json({err:false,message:"Pengingat berhasil dibuat",reminder:task})
    }else{
        res.json({err:true,message:"Data tidak valid"})
    }  
}
async function updateTask(req, res){
    Task.findOne({_id:req.params.id})
    .then(async(task)=>{
        if(task.user==req.body.userjwt){        
            if(req.body.user)delete req.body.user;
            const newTask=req.body;
            await Task.findByIdAndUpdate(req.params.id,newTask)
            // Bersihkan Task Queeue de Timeouts
            // Kemudian konsultasikan dengan DB dan buat batas waktu baru:
            deleteAllTimeOuts()
            res.json({message:'Data telah diperbarui'})
        }else{
            res.json({message:'Invalid user'})
        }
    })
    .catch(()=>{
        res.json({err:true,message:'Data tidak ditemukan'})
    })  
}
async function deleteTask(req, res){
    Task.findOne({_id:req.params.id})
    .then(async (task)=>{        
        if(task.user==req.body.userjwt){        
            await Task.findByIdAndRemove(req.params.id)
            deleteAllTimeOuts()
            res.json({err:false,message:'Pengingat dihapus'})
        }else{
            res.json({err:true,message:'Invalid user'})
        }
    })
    .catch(()=>{
        res.json({err:true,message:'Data tidak ditemukan'})
    })    
}


//Auth:

async function login(req,res) {
    const {username,password}=req.body;

    let user = await Users.findOne({username})
    if (user!=null && user.password===password){
        //generate token:
        const token=await auth.sign({user:user.username})
        res.status(200).send({
            err:false,
            token:token
        })
    }else{
        res.status(401).send({
            err:true,
            message: 'Username atau password anda salah'
        })
    }
}
async function register(req,res){
    const {username,password}=req.body;
    let userExist = await Users.findOne({username})
    if(!userExist){
        const user = new Users({username,password})
        await user.save();     
        res.json({err:false,message:"User berhasil dibuat!"});
    }else{
        res.json({err:true,message:"User sudah ada, silahkan gunakan nama lain."})
    }
}
function secure(req, res, next) {
    auth.logeado(req)
    .then(() => {
        //Jika kredensial benar, itu terjadi pada middleware berikut
        next();
    })
    .catch(() => {
        //Jika tidak valid, kirim 401
        res.status(401).send({
            message: "Tidak diizinkan",
        });
    });
}


//Whatsapp:
async function sendWA(number,message){
    
    number=number+"@c.us";

    clientWA = clientWA || await wa.create()

    setTimeout(async ()=>{
        return await clientWA.sendText(number, message);
    },2000)
    return true
}


//Cron

async function createCron(time,phone,description) {
    let newTime = time-new Date().getTime()
    let idTime
    if (newTime>=0) {
        idTime=setTimeout(async ()=>{
            sendWA(phone,description)
            await Task.findOneAndDelete({time,phone,description})
            console.log(`Mengirim pesan "${description}" ke "${phone}"`);
        },newTime)
        timeOutIDs.push(idTime)
    }else{
        await Task.findOneAndDelete({time,phone,description})
    }
}
async function deleteAllTimeOuts(){
    timeOutIDs.forEach((element)=>{
        clearTimeout(element)
    })
    init()
}

//menu

// async function Menu(message){
//    clientWA = clientWA || await wa.create()

//    client.onMessage(async message => {
//    if (message.body === '^menu') {
//      await clientWA.sendText(message.from, 'Menu : 1. Jadwal Piket Hari ini');
//}

//Init

async function init(){
    clientWA = clientWA || await wa.create()

    reminders= await Task.find()
    reminders.forEach(element => {        
        createCron(element.time,element.phone,element.description)
    });

}


module.exports = router;
