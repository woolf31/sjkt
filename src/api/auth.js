const jwt=require('jsonwebtoken')
const jwtSecreto=process.env.SecretJWT

function sign(data){
    //buat sesi:
    return jwt.sign(data,jwtSecreto,{ expiresIn: 604800 })
}

async function logeado(req){
    const autorizacion = req.headers.authorization || "";
    
    const token = getToken(autorizacion);
    
    const tokenDecodificado = jwt.verify(token, jwtSecreto);
    
    if(tokenDecodificado.user){
        req.body.userjwt=tokenDecodificado.user
    }else{
        throw new Error('Token tidak cocok')
    }
    return tokenDecodificado
}

function getToken(auth){
    if (!auth) {
		throw new Error('Token tidak ditemukan')
	}
	if (auth.indexOf("Bearer ") === -1) {
		throw new Error('Format token tidak valid')
    }
	return auth.replace("Bearer ", "");
}


module.exports={sign,logeado}